import {Component, Inject, OnInit, TemplateRef} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/models/user.model';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'wfm-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})
export class RegistrationComponent implements OnInit {

  regForm: FormGroup;
  user: User;
  isRegistrated = false;
  constructor(private usersService: UsersService,
              private router: Router) {
  }

  ngOnInit() {
    this.user = new User();

    this.regForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email] /*this.forbiddenEmails.bind(this)*/),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'name': new FormControl('', [Validators.required]),
      'agree': new FormControl(false, [Validators.requiredTrue]),
    });
  }

  onSubmit() {
    if (!this.forbiddenEmails(this.regForm.value.email)) {
      this.usersService.createNewUser(this.regForm.value)
        .subscribe(() => {
          this.router.navigate(['/login'], {
            queryParams: {
              nowCanLogin: true
            }
          });
        });
    }
    this.isRegistrated = true;

  }

  //
  forbiddenEmails(control: FormControl): Promise<any> {
    return new Promise((resolve, reject) => {
      this.usersService.getUserByEmail(control.value)
        .subscribe((user: User) => {
          if (user) {
            resolve({forbiddenEmail: true});
          } else {
            resolve(null);
          }
        });
    });
  }

  isRegistratedChange() {
    this.isRegistrated = false;
  }

}
