import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/models/user.model';
import {Message} from '../../shared/models/message.model';
import {AuthService} from '../../shared/services/auth.service';
import {ActivatedRoute, Params, Router} from '@angular/router';


@Component({
  selector: 'wfm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  authForm: FormGroup;
  message: Message;
  userLogined: User;

  constructor(private usersService: UsersService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.message = new Message('danger', '');

    this.route.queryParams.subscribe((params: Params) => {
      if (params ['nowCanLogin']) {
        console.log('few', params);
        this.showMessage({
          text: 'Now you can authorizated',
          type: 'success'
        });
      }
    });


    this.authForm = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.email]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6)])
    });
  }


  private showMessage(message: Message) {
    this.message = message;
    window.setTimeout(() => {
      this.message.text = '';
    }, 5000);

  }

  onSubmit() {
    const authFormData = this.authForm.value;

    this.usersService.getUserByEmail(authFormData.email)
      .subscribe((user: User[]) => {
        console.log(user);
        if (user.length > 0) {
          this.userLogined = user[0];
          console.log(this.userLogined.password);
          console.log(authFormData.password);
          if (this.userLogined.password === authFormData.password) {
            // this.message.text = '';
            window.localStorage.setItem('user', JSON.stringify(this.userLogined));
            this.authService.login();
            this.router.navigate(['/system', 'products']);
          } else {
            console.log('lfdmpld');
          }
        }
      });
  }

}
