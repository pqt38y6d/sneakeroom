import {Injectable, OnInit} from '@angular/core';
import {Basket} from '../models/basket';

@Injectable()
export class AddToBasketService implements OnInit {
  basket: Basket;


  ngOnInit(): void {
    this.loadBasket();
  }

  constructor() {
  }


  addToBasket(id: number) {
    if (!this.isExistsInBasket(id)) {
      this.basket.items.push(id);
      localStorage.setItem('basket', JSON.stringify(this.basket));
    }
  }

  isExistsInBasket(id: number) {
    this.loadBasket();
    if (this.basket.items == null) {
      return false;
    }
    const index = this.basket.items.findIndex(item => item === id);
    return index >= 0;
  }

  removeFromBasket(id: number) {
    if (this.isExistsInBasket(id)) {
      const index = this.basket.items.findIndex(item => item === id);
      this.basket.items.splice(index, 1);
      localStorage.setItem('basket', JSON.stringify(this.basket));
    }


  }

  private createBasket() {
    this.basket = new Basket();
    window.localStorage.setItem('basket', JSON.stringify(this.basket));
  }

    loadBasket() {
    this.basket = JSON.parse(window.localStorage.getItem('basket'));
    if (this.basket === null) {
      this.createBasket();
    }
  }


}
