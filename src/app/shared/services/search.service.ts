import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {ProductsListComponent} from '../../system/products-list/products-list.component';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable()
export class SearchService {

  private messageSource = new BehaviorSubject<string>('');
  currentMeaasge = this.messageSource.asObservable();

  constructor(){}

  changeMessage(message: string){
    this.messageSource.next(message);
  }




}
