import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ProductModel} from '../models/product.model';
import {Observable} from 'rxjs/index';

@Injectable()
export class ProductRestService {

  constructor(private httpClient: HttpClient) {
  }

  public getProductList(): Observable<ProductModel[]> {
    return this.httpClient.get<ProductModel[]>('/products',
      {
        observe: 'body'
      });
  }

  public getProductById(id: number): Observable<ProductModel> {
    return this.httpClient.get<ProductModel>('/products/' + id);
  }

  getByPage(page: number, size: number): Observable<any> {
    return this.httpClient.get('/products', {
      params: new HttpParams().set('_page', page.toString()).set('_limit', size.toString()),
      observe: 'response'
    });
  }

  getByPageWithParams(page: number, size: number, query: string): Observable<any> {
    return this.httpClient.get('/products', {
      params: new HttpParams().set('_page', page.toString()).set('_limit', size.toString()).set('q', query),
      observe: 'response'
    });
  }


  baseUrl = 'http://localhost:3000';
  params = new HttpParams();


  getProductsByParams(requestParams: string) {
    return this.httpClient.get<ProductModel[]>(this.baseUrl + '/products?' + requestParams);
  }

  searchProducts(requestParams: string) {
    return this.httpClient.get<ProductModel[]>(this.baseUrl + '/products?q=' + requestParams);
  }

  queryParams(data: object[]) {
    this.params = new HttpParams();
    data.forEach(item => {
      this.params = this.params.append(item[0], item[1]);
    });
    return this.params;
  }

  //
  // public getXCount(): Observable<ProductModel> {
  //   return this.http.get<any>('/products', {observe: 'response'})
  //     .subscribe(resp => {
  //       console.log(resp.headers.get('X-Total-Count'));
  //     });
  // }


  // public addProduct(product: ProductModel): Observable<ProductModel> {
  //   return this.httpClient.post<ProductModel>('api/products', product);
  // }
  //
  // public updateProduct(product: ProductModel): Observable<ProductModel> {
  //   return this.httpClient.put<ProductModel>('api/products/', product);
  // }
  //
  // public deleteProduct(productId: number): Observable<any> {
  //   return this.httpClient.delete(`api/products/${productId}`);
  // }
}
