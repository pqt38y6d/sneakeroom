import {Injectable} from '@angular/core';

import {User} from '../models/user.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) {
  }

  getUserByEmail(email: string): Observable<any> {
    return this.http.get('/users?email=' + email, {
      observe: 'body'
    });
  }

  login(email: string, password: string): Observable<any> {
    return this.http.post('/auth/login', {
      email: email,
      password: password
    });
  }

  createNewUser(user: User): Observable<User> {
    return this.http.post<User>('/users', user);
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<User>('/users/' + user.id, user);
  }
}
