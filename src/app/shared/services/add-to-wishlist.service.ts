import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {UsersService} from './users.service';
import {ProductRestService} from './product-rest.service';

@Injectable()
export class AddToWishlistService {

  constructor(private usersService: UsersService,
              private productService: ProductRestService) {
    this.userUpdate();
  }

  user: User;

  addToWishlist(id: number) {
    this.userUpdate();
    if (!this.isExistsInWishlist(id)) {
      this.user.wishList.push(id);
      localStorage.setItem('user', JSON.stringify(this.user));
      this.usersService.updateUser(this.user).subscribe(item => {

      });
    }
  }

  removeFromWishlist(id: number) {
    if (this.isExistsInWishlist(id)) {
      const index = this.user.wishList.findIndex(item => item === id);
      this.user.wishList.splice(index, 1);
      localStorage.setItem('user', JSON.stringify(this.user));
      this.usersService.updateUser(this.user).subscribe(item => {

      });

    }
  }

  isExistsInWishlist(id: number) {
    return this.user.wishList.findIndex(item => {
      return item === id
    }) != -1;
  }

  userUpdate() {
    this.user = JSON.parse(window.localStorage.getItem('user'));
  }

}
