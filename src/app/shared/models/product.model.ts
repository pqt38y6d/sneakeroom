export class ProductModel {
  constructor(
    public title: string,
    public image: string,
    public price: number,
    public id?: number,
    public category?: string,
    public color?: string,
    public description?: string
  ) {}
}
