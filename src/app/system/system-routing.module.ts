import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SystemComponent} from './system.component';
import {ProductsListComponent} from './products-list/products-list.component';
import {HistoryPageComponent} from './history-page/history-page.component';
import {ProductInformationComponent} from './products-list/product-page/product-information/product-information.component';
import {UserAccountComponent} from './user-account/user-account.component';
import {UserDataComponent} from './user-account/user-data/user-data.component';
import {WishlistComponent} from './user-account/wishlist/wishlist.component';
import {ShoppingCartComponent} from './user-account/shopping-cart/shopping-cart.component';


const routes: Routes = [
  {
    path: 'system', component: SystemComponent, children: [
      {path: 'products', component: ProductsListComponent},

      {
        path: 'user-account', component: UserAccountComponent, children: [
          {path: 'user-data', component: UserDataComponent},
          {path: 'wishlist', component: WishlistComponent},
          {path: 'shopping-cart', component: ShoppingCartComponent}
        ]
      },

      {path: 'products/:id', component: ProductInformationComponent},

      {path: 'history', component: HistoryPageComponent}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class SystemRoutingModule {
}




