import {Component, OnInit} from '@angular/core';
import {User} from '../../../../shared/models/user.model';
import {AuthService} from '../../../../shared/services/auth.service';
import {Router} from '@angular/router';
import {ProductRestService} from '../../../../shared/services/product-rest.service';
import {ProductsListComponent} from '../../../products-list/products-list.component';
import {SearchService} from '../../../../shared/services/search.service';

@Component({
  selector: 'wfm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService,
              private router: Router,
              private productRestService: ProductRestService,
              private searchService: SearchService) {
  }

  addedInformation = 'FREE WORLDWIDE SHIPPING';
  user: User;
  private productList: ProductsListComponent;

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('user'));
  }

  onLogOut() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  openSignature() {
    this.searchService.changeMessage('signature');
  }

  openInnovation() {
    this.searchService.changeMessage('innovation');
  }

  toMainPage() {
    this.searchService.changeMessage('');
  }
}
