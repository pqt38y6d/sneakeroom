import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SearchService} from '../../../../shared/services/search.service';
import {query} from '@angular/animations';
import {ProductRestService} from '../../../../shared/services/product-rest.service';
import {ProductModel} from '../../../../shared/models/product.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {


  @Output() onSearch = new EventEmitter<string>();



  constructor(private productRestService: ProductRestService,
              private searchService: SearchService,
              private router: Router) { }

  query: string;

  ngOnInit() {
  }





  search(query: string) {
    this.searchService.changeMessage(query);
    this.query = '';
    this.router.navigateByUrl('/system/products');

  }
}
