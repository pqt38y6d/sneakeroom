import {Component, OnInit} from '@angular/core';
import {ProductRestService} from '../../../shared/services/product-rest.service';
import {ProductModel} from '../../../shared/models/product.model';
import {User} from '../../../shared/models/user.model';
import {AddToWishlistService} from '../../../shared/services/add-to-wishlist.service';

@Component({
  selector: 'wfm-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.less']
})
export class WishlistComponent implements OnInit {

  constructor(private productService: ProductRestService,
              private wishListService: AddToWishlistService) {
  }

  sum: number;
  loadedProducts: ProductModel[];
  currentUser: User;


  // loadedProductsId: number[];


  ngOnInit() {
    this.loadedProducts = [];
    this.getItems();
    this.loadProducts();
  }

  private loadProducts() {
    this.loadedProducts = [];
    this.currentUser.wishList.forEach(item => {
      this.productService.getProductById(item).subscribe(loaded => {
        this.loadedProducts.push(loaded);
      });
    });
  }

  getItems() {
    this.currentUser = JSON.parse(window.localStorage.getItem('user'));
  }


  deleteItem(id: number) {
    this.wishListService.removeFromWishlist(id);
    const index = this.loadedProducts.findIndex(item => {
      return item.id === id;
    });
    this.loadedProducts.splice(index, 1);
  }

  onChanged() {
    this.getItems();
    this.loadProducts();
  }

}
