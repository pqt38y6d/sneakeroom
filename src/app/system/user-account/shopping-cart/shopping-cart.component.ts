import {Component, OnInit} from '@angular/core';
import {ProductModel} from '../../../shared/models/product.model';
import {ProductRestService} from '../../../shared/services/product-rest.service';
import {Basket} from '../../../shared/models/basket';
import {AddToBasketService} from '../../../shared/services/add-to-basket.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'wfm-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.less']
})
export class ShoppingCartComponent implements OnInit {

  constructor(private productService: ProductRestService,
              private addToBasketService: AddToBasketService,
              private _formBuilder: FormBuilder) {
  }

  basket: Basket;
  sum: number;
  loadedProducts: ProductModel[];
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;



  ngOnInit() {
    this.loadedProducts = [];
    this.getItems();
    this.loadProducts();
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  private loadProducts() {
    this.basket.items.forEach(id => {
      this.productService.getProductById(id).subscribe(res => {
        this.loadedProducts.push(res);
        this.finallyPrice();
      });
    });
    // this.productService.getProductById().subscribe(res => {
    //   this.loadedProducts = res;
    //   this.finallyPrice();
    // });
    // this.getItems();
  }

  getItems() {
    this.basket = JSON.parse(window.localStorage.getItem('basket'));
  }

  finallyPrice() {
    this.sum = 0;
    this.loadedProducts.forEach(item => {
      this.sum = this.sum + item.price;
    });
    return this.sum;
  }

  deleteItem(id: number) {
    this.addToBasketService.removeFromBasket(id);
    const index = this.loadedProducts.findIndex(item => {
      return item.id === id;
    });
    this.loadedProducts.splice(index, 1);
    this.finallyPrice();
  }
}
