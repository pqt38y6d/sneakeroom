import {Component, destroyPlatform, OnChanges, OnInit, ViewChild} from '@angular/core';
import {ProductModel} from '../../shared/models/product.model';
import {ProductRestService} from '../../shared/services/product-rest.service';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent} from '@angular/material';
import {ProductInformationComponent} from './product-page/product-information/product-information.component';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {SearchService} from '../../shared/services/search.service';
import {User} from '../../shared/models/user.model';
import {HttpResponse} from '@angular/common/http';

// @ts-ignore
@Component({
  selector: 'wfm-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.less']
})
export class ProductsListComponent implements OnInit{


  private products: ProductModel[] = [];
  dataSource = new MatTableDataSource<ProductModel>(this.products);
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  dataLength: number;
  private changePageEvent: PageEvent;
  defaultPageSize = 4;
  pageSizeOptions: number[] = [4, 8, 12, 24];
  filterForm: FormGroup;
  httpResponse;

  colors = [
    {id: 1, color: 'black'},
    {id: 2, color: 'grey'},
    {id: 3, color: 'beige'},
    {id: 4, color: 'white'}

  ];


  categories = [
    {id: 1, category: 'signature'},
    {id: 2, category: 'innovation'}
  ];
  private filterRequest: object[];
  private priceArray = [];


  search: string;

  constructor(
    private productRestService: ProductRestService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private searchService: SearchService,
  ) {
    const colorsFormControls = this.colors.map(control => new FormControl(false));
    const categoriesFormControls = this.categories.map(control => new FormControl(false));


    this.filterForm = this.fb.group({
      colorPreferences: new FormArray(colorsFormControls),
      categoryPreferences: new FormArray(categoriesFormControls),
      priceFrom: new FormControl(''),
      priceTo: new FormControl('')
    });


  }

  ngOnInit() {
    this.getSearchQuery();
    this.changePageEvent = new PageEvent();
    console.log('event is created');
    this.changePageEvent.pageIndex = 0;
    this.changePageEvent.pageSize = this.defaultPageSize;
    this.changePage(this.changePageEvent);
  }


  getSearchQuery() {

  }

  private getPage(page: number, limit: number) {
    this.productRestService.getByPage(page, limit).subscribe(result => {
      this.products = result.body;
      this.dataLength = result.headers.get('X-Total-Count');
    });
  }

  private resetFilters() {
    this.getData();
    this.search = '';
    this.filterForm.reset({priceFrom: '', PriceTo: ''});
  }

  private getData() {
    this.productRestService.getProductList()
      .subscribe(data => {
        this.products = data;
      });
  }


  openDialog(item: ProductModel) {
    const dialogRef = this.dialog.open(ProductInformationComponent, {
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  changePage($event: PageEvent) {
    this.changePageEvent = $event;
        this.changePageEvent.pageIndex += 1;
        this.searchService.currentMeaasge.subscribe(query => {
          console.log(query);
          if (query.length != 1)
          {
            // console.log(this.changePageEvent.pageIndex, this.changePageEvent.pageSize);
            this.productRestService.getByPageWithParams(this.changePageEvent.pageIndex, this.changePageEvent.pageSize, query).subscribe(result => {
              if(result.body.length == 0){
                this.paginator.firstPage();
              }
              // console.log(result);
              this.products = result.body;
              this.dataLength = result.headers.get('X-Total-Count');
            });
          } else {
            this.getPage(this.changePageEvent.pageIndex, this.changePageEvent.pageSize);
          }

        });
  }


  submit() {
    const selectedColorPreferences = this.filterForm.value.colorPreferences
      .map((checked, index) => checked ? [Object.keys(this.colors[index])[1].toString(), this.colors[index].color.toString()] : null)
      .filter(value => value !== null);
    const selectedCategoryPreferences = this.filterForm.value.categoryPreferences
      .map((checked, index) => checked ? [Object.keys(this.categories[index])[1].toString(), this.categories[index].category.toString()] : null)
      .filter(value => value !== null);
    const selectedPriceFrom = this.filterForm.value.priceFrom;
    const selectedPriceTo = this.filterForm.value.priceTo;
    if (selectedPriceFrom !== '' && selectedPriceFrom !== undefined && selectedPriceFrom !== null) {
      this.priceArray.push(['price_gte', selectedPriceFrom]);
    }
    if (selectedPriceTo !== '' && selectedPriceTo !== undefined && selectedPriceTo !== null) {
      this.priceArray.push(['price_lte', selectedPriceTo]);
    }
    if (this.priceArray.filter(value => value !== null)) {
      this.filterRequest = [...selectedCategoryPreferences, ...selectedColorPreferences, ...this.priceArray];
    } else {
      this.filterRequest = [...selectedCategoryPreferences, ...selectedColorPreferences];

    }

    console.log(this.productRestService.queryParams(this.filterRequest).toString());
    this.productRestService.getProductsByParams(this.productRestService.queryParams(this.filterRequest).toString());
    this.priceArray = [];
    this.productRestService.getProductsByParams(this.productRestService.queryParams(this.filterRequest).toString()).subscribe((res) => {
      this.products = res;
    });
  }


  startSearch(search: string) {
    this.productRestService.searchProducts(search).subscribe(products => {
      this.products = products;
    });
  }



}




