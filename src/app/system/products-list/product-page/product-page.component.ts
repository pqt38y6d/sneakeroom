import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductModel} from '../../../shared/models/product.model';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {AddToWishlistService} from '../../../shared/services/add-to-wishlist.service';
import {AddToBasketService} from '../../../shared/services/add-to-basket.service';



@Component({
  selector: 'wfm-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.less']
})
export class ProductPageComponent implements OnInit {

  constructor(
    private router: Router,
    private addToBasketService: AddToBasketService,
    private addToWishlistService: AddToWishlistService
  ) {}
  @Input() product: ProductModel;


  ngOnInit() {
  }

  @Output() onChanged = new EventEmitter<boolean>();



  isOnWishlistPage() {
   return this.router.url === '/system/user-account/wishlist';
  }

  addToCart() {
    this.addToBasketService.addToBasket(this.product.id);
  }

  removeFromWishlist(increased:any) {
    this.addToWishlistService.removeFromWishlist(this.product.id);
    this.onChanged.emit();
  }

  isInBasket(): boolean {
    return this.addToBasketService.isExistsInBasket(this.product.id);
  }



}
