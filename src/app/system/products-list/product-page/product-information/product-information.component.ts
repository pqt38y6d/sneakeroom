import {Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductModel} from '../../../../shared/models/product.model';
import {ProductRestService} from '../../../../shared/services/product-rest.service';
import {MAT_DIALOG_DATA} from '@angular/material';
import {AddToBasketService} from '../../../../shared/services/add-to-basket.service';
import {AddToWishlistService} from '../../../../shared/services/add-to-wishlist.service';

@Component({
  selector: 'wfm-product-information',
  templateUrl: './product-information.component.html',
  styleUrls: ['./product-information.component.less']
})
export class ProductInformationComponent implements OnInit {
  //
  id: number;

  constructor(private route: ActivatedRoute,
              @Inject(MAT_DIALOG_DATA) public sneaker: ProductModel,
              private productService: ProductRestService,
              private addToBasketService: AddToBasketService,
              private addToWishlistService: AddToWishlistService) {

  }

  ngOnInit() {
    this.isInWishlist();
    this.isInBasket();
  }

  removeFromCart() {
    this.addToBasketService.removeFromBasket(this.sneaker.id);
  }

  addToCart() {
    this.addToBasketService.addToBasket(this.sneaker.id);
  }

  isInWishlist() {
    return this.addToWishlistService.isExistsInWishlist(this.sneaker.id);
  }

  isInBasket() {
    return this.addToBasketService.isExistsInBasket(this.sneaker.id);
  }

  addToWishlist() {
    this.addToWishlistService.addToWishlist(this.sneaker.id);
  }

  removeFromWishlist() {
    this.addToWishlistService.removeFromWishlist(this.sneaker.id);
  }
}
