import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {CommonModule} from '@angular/common';
import {SystemRoutingModule} from './system-routing.module';
import { HistoryPageComponent } from './history-page/history-page.component';
import {SystemComponent} from './system.component';
import {ProductsListComponent} from './products-list/products-list.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { ProductPageComponent } from './products-list/product-page/product-page.component';
import { ProductInformationComponent } from './products-list/product-page/product-information/product-information.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { WishlistComponent } from './user-account/wishlist/wishlist.component';
import { ShoppingCartComponent } from './user-account/shopping-cart/shopping-cart.component';
import { UserDataComponent } from './user-account/user-data/user-data.component';
import { SearchComponent } from './shared/components/search/search.component';
import {SearchPipe} from '../shared/pipes/search.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SystemRoutingModule],
  declarations: [
    HistoryPageComponent,
    ProductsListComponent,
    SystemComponent,
    HeaderComponent,
    ProductPageComponent,
    ProductInformationComponent,
    UserAccountComponent,
    WishlistComponent,
    ShoppingCartComponent,
    UserDataComponent,
    SearchComponent,
    SearchPipe
  ]
})

export class  SystemModule {}
