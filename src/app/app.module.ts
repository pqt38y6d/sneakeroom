import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AuthModule} from './auth/auth.module';
import {AppRoutingModule} from './app-routing.module';
import {SharedModule} from './shared/shared.module';
import {SystemModule} from './system/system.module';
import {HttpClientModule} from '@angular/common/http';
import {AddToBasketService} from './shared/services/add-to-basket.service';
import {AddToWishlistService} from './shared/services/add-to-wishlist.service';
import {ProductRestService} from './shared/services/product-rest.service';
import {AuthService} from './shared/services/auth.service';
import {UsersService} from './shared/services/users.service';
import {SearchService} from './shared/services/search.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AuthModule,
    AppRoutingModule,
    SharedModule,
    SystemModule,
    HttpClientModule,
  ],
  providers: [UsersService, AuthService, ProductRestService, AddToWishlistService, AddToBasketService, SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
